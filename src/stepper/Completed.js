import React from 'react'

function Completed() {
    return (
        <div>
            All steps completed - you're finished
        </div>
    )
}

export default Completed