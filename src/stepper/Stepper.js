import React, { useState, useCallback, useEffect } from 'react'
import Chevron from './Chevron'
import Completed from './Completed'
import StepOneContent from './StepOneContent'
import StepThreeContent from './StepThreeContent'
import StepTwoContent from './StepTwoContent'

function Stepper({ stepsNames }) {

  const defaultSteps = stepsNames.map((step, index) => {
    return {
      text: step,
      optional: index === 0,
      active: index === 0,
      completed: false
    }
  });

    const [ currentStep, setCurrentStep ] = useState(1)
    const [ progress, setProgress ] = useState(stepsNames.map((item, id) => ({
      ...item,
      id,
      active: !id,
      finished: false
    })));

    const onClick = useCallback((step) => {
      const activeItem = progress.find(item => item.active);
      const newProgress = progress.map(item => ({
        ...item,
        active: item.id === activeItem.id + step,
        finished: item.finished || item.id === activeItem.id
        })
      )
      setProgress(newProgress)
    })

    const [ currentStepArr, setCurrentStepArr] = useState(updateStep(0, defaultSteps))


    const stepsList = currentStepArr.map((item, index) => (
        <div className="step" key={index}>
            <div className={`bullet ${item.active ? "active" : ""} ${item.completed ? "completed" : ""}`}>{item.completed ? <Chevron width={12} fill={"#fff"} /> : index + 1}</div>
            <div className={`step-text ${item.active ? "active" : ""} ${item.completed ? "completed" : ""}`}>{item.text}</div>
        </div>
    ))

    const handleClick = useCallback((step) => {
      setCurrentStep(currentStep + step)
      onClick(step)
    }, [currentStep]);

    const handleReset = useCallback(()=> {
      setCurrentStep(1)
    }, []);

    useEffect(() => {
          const newSteps = updateStep((currentStep-1), defaultSteps)
          setCurrentStepArr(newSteps)
    }, [currentStep]
    )

   function updateStep(stepNumber, steps) {
        const newSteps = [...steps];
        let stepCounter = 0;

        while (stepCounter < newSteps.length) {
          // Current step
          if (stepCounter === stepNumber) {
            newSteps[stepCounter] = {
              ...newSteps[stepCounter],
              optional: true,
              active: true,
              completed: false
            };
            stepCounter++;
          }
          // Past step
          else if (stepCounter < stepNumber) {
            newSteps[stepCounter] = {
              ...newSteps[stepCounter],
              optional: false,
              active: true,
              completed: true
            };
            stepCounter++;
          }
          // Future step
          else {
            newSteps[stepCounter] = {
              ...newSteps[stepCounter],
              optional: false,
              active: false,
              completed: false
            };
            stepCounter++;
          }
        }

        return newSteps;
      }

    // useEffect(() => {
    //     const currentStep = updateStep()
    // }, [currentStep])

  const steps = {
      1: StepOneContent,
      2: StepTwoContent,
      3: StepThreeContent,
  }

    return (
        <div className="container">
            <div id="stepProgressBar">
                {stepsList}
            </div>
            <div id="main">
                <div className="step-content">
                  {steps[currentStep] ? React.createElement(steps[currentStep]) : <Completed/>}
                </div>
                <div className="buttons">
                    <button className={currentStep === currentStepArr.length + 1 ? "hidden" : "back"} disabled={currentStep < 2 && true} onClick={() => handleClick(-1)}>Back</button>
                    <button className={(currentStep < 2) ? "hidden" : (currentStep >= currentStepArr.length) ? "hidden" : ""} onClick={handleClick}>Skip</button>
                    <button className={currentStep >= currentStepArr.length ? "hidden" : ""} disabled={currentStep >= currentStepArr.length && true} onClick={() => handleClick(1)}>Next</button>
                    <button className={currentStep !== currentStepArr.length ? "hidden" : ""} onClick={() => handleClick(1)}>Finish</button>
                    <button className={currentStep === currentStepArr.length + 1 ? "back" : "hidden"} onClick={handleReset}>Reset</button>
                </div>
            </div>
        </div>
    )
}


export default Stepper
