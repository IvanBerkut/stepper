import './App.css';
import Stepper from './stepper/Stepper';

const stepsNames = [
  "Select campaign settings",
  "Create an ad group",
  "Create an ad"
]

function App() {
  return (
    <div className="stepper-container-horizontal">
      <Stepper stepsNames={stepsNames}/>
    </div>
  );
}

export default App;
